package editor;

import java.io.IOException;
import java.io.File;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Container;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Gui extends JFrame implements ActionListener, Runnable {

    public List<String> dirs;
    public List<String> procedurs;
    public List<String> linesKeyB;
    public List<String> linesKeyA;
    public List<String> linesAdd;
    Thread tLoadConfig;
    Thread tSaveConfig;
    ReadWriteFile rwf1;
    File file;
    JCheckBox equals = new JCheckBox("equals");
    JLabel label1 = new JLabel("Main dir:", SwingConstants.LEFT);
    JLabel label2 = new JLabel("Procedurs:", SwingConstants.LEFT);
    JLabel label3 = new JLabel("Line before:", SwingConstants.LEFT);
    JLabel label4 = new JLabel("Lines for write:", SwingConstants.LEFT);
    JLabel label5 = new JLabel("Line after:", SwingConstants.LEFT);
    JTextArea textArea1 = new JTextArea(2, 40);
    JTextArea textArea2 = new JTextArea(2, 40);
    JTextArea textArea3 = new JTextArea(2, 40);
    JTextArea textArea4 = new JTextArea(2, 40);
    JTextArea textArea5 = new JTextArea(2, 40);
    JButton button1 = new JButton("Load");
    JButton buttonSave = new JButton("Save As");
    JButton button2 = new JButton("Find");
    JButton button3 = new JButton("Write");
    JButton button4 = new JButton("Close");
    JButton button5 = new JButton("Replace");
    JButton button6 = new JButton("Remove line");
    JButton button7 = new JButton("Comment line");
    JButton button8 = new JButton("Uncomment line");
    JButton button9 = new JButton("LineS delete");
    JScrollPane scrollPane1 = new JScrollPane(textArea1);
    JScrollPane scrollPane2 = new JScrollPane(textArea2);
    JScrollPane scrollPane3 = new JScrollPane(textArea3);
    JScrollPane scrollPane4 = new JScrollPane(textArea4);
    JScrollPane scrollPane5 = new JScrollPane(textArea5);
    JSplitPane splitPane1 = new JSplitPane();
    JFileChooser fileChooser = new JFileChooser();

    @Override
    public void run() {
        dirs = new ArrayList<>();
        procedurs = new ArrayList<>();
        linesKeyB = new ArrayList<>();
        linesKeyA = new ArrayList<>();
        linesAdd = new ArrayList<>();
        setSize(640, 480);
        setTitle("Cod Editor");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Panel panelMain = new Panel();
        Panel panelLeft = new Panel();
        Panel panelRight = new Panel();
        Panel panelButtons = new Panel();
        Panel panelConfigButtons = new Panel();
        //Container contentPane = panelLeft.getContentPane();
        panelMain.setLayout(new FlowLayout());
        panelLeft.setLayout(new GridLayout(5, 0));
        panelRight.setLayout(new GridLayout(5, 0));
        panelButtons.setLayout(new GridLayout(0, 4));
        panelConfigButtons.setLayout(new GridLayout(0, 1));
        panelLeft.add(label1);
        panelLeft.add(label2);
        panelLeft.add(label3);
        panelLeft.add(label4);
        panelLeft.add(label5);
        panelRight.add(scrollPane1);
        panelRight.add(scrollPane2);
        panelRight.add(scrollPane3);
        panelRight.add(scrollPane4);
        panelRight.add(scrollPane5);
        panelConfigButtons.add(button1);
        panelConfigButtons.add(buttonSave);
        panelConfigButtons.add(button4);
        panelButtons.add(button2);
        panelButtons.add(button3);
        panelButtons.add(button6);
        panelButtons.add(button5);
        panelButtons.add(button9);
        panelButtons.add(button7);
        panelButtons.add(button8);
        splitPane1.setLeftComponent(panelLeft);
        splitPane1.setRightComponent(panelRight);
        panelMain.add(splitPane1);
        panelMain.add(equals);
        panelMain.add(panelConfigButtons);
        panelMain.add(panelButtons);
        add(panelMain);
        button1.setActionCommand("load");
        button1.addActionListener(this);
        buttonSave.setActionCommand("save");
        buttonSave.addActionListener(this);
        button2.setActionCommand("find");
        button2.addActionListener(this);
        button3.setActionCommand("write");
        button3.addActionListener(this);
        button6.setActionCommand("remove");
        button6.addActionListener(this);
        button5.setActionCommand("replace");
        button5.addActionListener(this);
        button9.setActionCommand("Lines delete");
        button9.addActionListener(this);
        button7.setActionCommand("comment");
        button7.addActionListener(this);
        button8.setActionCommand("uncomment");
        button8.addActionListener(this);
        button4.setActionCommand("close");
        button4.addActionListener(this);
        setVisible(true);
    }

    public void loadConfig() throws IOException {
        //LoadLines ll = new LoadLines(dirs, procedurs, linesKey, linesAdd, "editor.load", "cp866");
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        int returnVal;

        if ("load".equals(event.getActionCommand())) {
            returnVal = fileChooser.showOpenDialog(this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                textArea1.setText(null);
                textArea2.setText(null);
                textArea3.setText(null);
                textArea4.setText(null);
                textArea5.setText(null);
                file = fileChooser.getSelectedFile();
                tLoadConfig = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Config.Load(file.getAbsolutePath(), "cp866");
                        } catch (IOException ex) {
                            Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                tLoadConfig.start();
                if (tLoadConfig.isAlive()) {
                    try {
                        tLoadConfig.join();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            dirs = Config.dirs;
            procedurs = Config.procedurs;
            linesKeyB = Config.linesKeyB;
            linesKeyA = Config.linesKeyA;
            linesAdd = Config.linesAdd;

            if (dirs != null) {
                for (int i = 0; i < dirs.size(); i++) {
                    if (i != 0) {
                        textArea1.append("\n");
                    }
                    textArea1.append(dirs.get(i));
                }
            }
            if (procedurs != null) {
                for (int i = 0; i < procedurs.size(); i++) {
                    if (i != 0) {
                        textArea2.append("\n");
                    }
                    textArea2.append(procedurs.get(i));
                }
            }
            if (linesKeyB != null) {
                for (int i = 0; i < linesKeyB.size(); i++) {
                    if (i != 0) {
                        textArea3.append("\n");
                    }
                    textArea3.append(linesKeyB.get(i));
                }
            }
            if (linesKeyA != null) {
                for (int i = 0; i < linesKeyA.size(); i++) {
                    if (i != 0) {
                        textArea5.append("\n");
                    }
                    textArea5.append(linesKeyA.get(i));
                }
            }
            if (linesAdd != null) {
                for (int i = 0; i < linesAdd.size(); i++) {
                    if (i != 0) {
                        textArea4.append("\n");
                    }
                    textArea4.append(linesAdd.get(i));
                }
            }
        } else if ("save".equals(event.getActionCommand())) {
            this.readFormData();
            returnVal = fileChooser.showOpenDialog(this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                file = fileChooser.getSelectedFile();
                Config.dirs = dirs;
                Config.procedurs = procedurs;
                Config.linesKeyB = linesKeyB;
                Config.linesKeyA = linesKeyA;
                Config.linesAdd = linesAdd;
                tSaveConfig = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Config.Save(file.getAbsolutePath(), "cp866");
                        } catch (IOException ex) {
                            Logger.getLogger(Gui.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                tSaveConfig.start();
            }
        } else if ("find".equals(event.getActionCommand())) {
            this.readFormData();

            int numFiles = 0;
            int numFilesFinded = 0;
            boolean inAllFilesFinded = true;
            StringBuilder withoutFiles = new StringBuilder();

            for (String mainDir : dirs) {
                String[] dir1 = new File(mainDir).list();
                for (int i = 0; i < dir1.length; i++) {
                    if (new File(mainDir + "/" + dir1[i]).isFile()) {
                        try {
                            System.out.println();
                            System.out.println(dir1[i]);
                            rwf1 = new ReadWriteFile(mainDir + "/" + dir1[i], mainDir + "/" + dir1[i], "Cp866");
                            numFiles++;
                            if ((linesKeyB.size() > 0) && !(linesKeyB.get(linesKeyB.size() - 1).equals(""))
                                    && rwf1.findLine(linesKeyB, procedurs, equals.isSelected())) {
                                numFilesFinded++;
                            } else if ((linesKeyA.size() > 0) && !(linesKeyA.get(linesKeyA.size() - 1).equals(""))
                                    && rwf1.findLine(linesKeyA, procedurs, equals.isSelected())) {
                                numFilesFinded++;
                            } else {
                                inAllFilesFinded = false;
                                withoutFiles.append(" " + dir1[i]);
                            }
                        } catch (IOException ioe) {
                            System.out.println("ERROR READ FILE: " + mainDir + "/" + dir1[i]);
                        }
                    }
                }
            }
            System.out.println("");
            System.out.println("numAllFiles: " + numFiles + " numFilesFinded: " + numFilesFinded);
            if (!inAllFilesFinded) {
                System.out.println("withoutFiles: " + withoutFiles.toString());
            }

        } else if ("write".equals(event.getActionCommand())) {
            this.readFormData();

            int numFiles = 0;
            int numFilesWrited = 0;
            boolean inAllFilesWrited = true;
            StringBuilder withoutFiles = new StringBuilder();

            for (String mainDir : dirs) {
                String[] dir1 = new File(mainDir).list();
                for (int i = 0; i < dir1.length; i++) {
                    if (new File(mainDir + "/" + dir1[i]).isFile()) {
                        try {
                            System.out.println(dir1[i]);
                            rwf1 = new ReadWriteFile(mainDir + "/" + dir1[i], mainDir + "/" + dir1[i], "Cp866");
                            numFiles++;
                            if ((linesKeyB.size() > 0) && !(linesKeyB.get(linesKeyB.size() - 1).equals(""))
                                    && rwf1.beforeLine(linesKeyB, linesAdd, procedurs, equals.isSelected())) {
                                numFilesWrited++;
                                System.out.println();
                            } else if ((linesKeyA.size() > 0) && !(linesKeyA.get(linesKeyA.size() - 1).equals(""))
                                    && rwf1.afterLine(linesKeyA, linesAdd, procedurs, equals.isSelected())) {
                                numFilesWrited++;
                                System.out.println();
                            } else {
                                inAllFilesWrited = false;
                                withoutFiles.append(" " + dir1[i]);
                            }
                        } catch (IOException ioe) {
                            System.out.println("ERROR READ FILE: " + mainDir + "/" + dir1[i]);
                        }
                    }
                }
            }
            System.out.println("");
            System.out.println("numAllFiles: " + numFiles + " numFilesWrited: " + numFilesWrited);
            if (!inAllFilesWrited) {
                System.out.println("withoutFiles: " + withoutFiles.toString());
            }
        } else if ("remove".equals(event.getActionCommand())) {
            this.readFormData();

            int numFiles = 0;
            int numFilesRemoved = 0;
            boolean inAllFilesRemoved = true;
            boolean inAllFilesChanged = true;

            for (String mainDir : dirs) {
                String[] dir1 = new File(mainDir).list();
                for (int i = 0; i < dir1.length; i++) {
                    if (new File(mainDir + "/" + dir1[i]).isFile()) {
                        try {
                            System.out.println(dir1[i]);
                            rwf1 = new ReadWriteFile(mainDir + "/" + dir1[i], mainDir + "/" + dir1[i], "Cp866");
                            numFiles++;
                            if (rwf1.removeLine(linesKeyB, procedurs, equals.isSelected())) {
                                numFilesRemoved++;
                                System.out.println();
                            } else if (rwf1.removeLine(linesKeyA, procedurs, equals.isSelected())) {
                                numFilesRemoved++;
                                System.out.println();
                            } else {
                                inAllFilesRemoved = false;
                            }
                        } catch (IOException ioe) {
                            System.out.println("ERROR READ FILE: " + mainDir + "/" + dir1[i]);
                        }
                    }
                }
            }
            System.out.println("");
            System.out.println("numAllFiles: " + numFiles + " numFilesRemoved: " + numFilesRemoved);
        } else if ("replace".equals(event.getActionCommand())) {
            this.readFormData();

            int numFiles = 0;
            int numFilesReplaced = 0;
            boolean inAllFilesReplaced = true;
            StringBuilder withoutFiles = new StringBuilder();

            for (String mainDir : dirs) {
                String[] dir1 = new File(mainDir).list();
                for (int i = 0; i < dir1.length; i++) {
                    if (new File(mainDir + "/" + dir1[i]).isFile()) {
                        try {
                            System.out.println(dir1[i]);
                            rwf1 = new ReadWriteFile(mainDir + "/" + dir1[i], mainDir + "/" + dir1[i], "Cp866");
                            numFiles++;
                            if ((linesKeyB.size() > 0) && !(linesKeyB.get(linesKeyB.size() - 1).equals(""))
                                    && rwf1.replace(linesKeyB, linesAdd, procedurs, equals.isSelected())) {
                                numFilesReplaced++;
                                System.out.println();
                            } else if ((linesKeyA.size() > 0) && !(linesKeyA.get(linesKeyA.size() - 1).equals(""))
                                    && rwf1.replace(linesKeyA, linesAdd, procedurs, equals.isSelected())) {
                                numFilesReplaced++;
                                System.out.println();
                            } else {
                                inAllFilesReplaced = false;
                                withoutFiles.append(" " + dir1[i]);
                            }
                        } catch (IOException ioe) {
                            System.out.println("ERROR READ FILE: " + mainDir + "/" + dir1[i]);
                        }
                    }
                }
            }
            System.out.println("");
            System.out.println("numAllFiles: " + numFiles + " numFilesReplaced: " + numFilesReplaced);
            if (!inAllFilesReplaced) {
                System.out.println("withoutFiles: " + withoutFiles);
            }
        } else if ("Lines delete".equals(event.getActionCommand())) {
            this.readFormData();

            int numFiles = 0;
            int numFilesRemoved = 0;
            boolean inAllFilesRemoved = true;
            boolean inAllFilesChanged = true;

            for (String mainDir : dirs) {
                String[] dir1 = new File(mainDir).list();
                for (int i = 0; i < dir1.length; i++) {
                    if (new File(mainDir + "/" + dir1[i]).isFile()) {
                        try {
                            System.out.println(dir1[i]);
                            rwf1 = new ReadWriteFile(mainDir + "/" + dir1[i], mainDir + "/" + dir1[i], "Cp866");
                            numFiles++;
                            if (rwf1.removeLines(linesAdd, procedurs)) {
                                numFilesRemoved++;
                                System.out.println();
                            } else {
                                inAllFilesRemoved = false;
                            }
                        } catch (IOException ioe) {
                            System.out.println("ERROR READ FILE: " + mainDir + "/" + dir1[i]);
                        }
                    }
                }
            }
            System.out.println("");
            System.out.println("numAllFiles: " + numFiles + " numFilesRemoved: " + numFilesRemoved);
        } else if ("close".equals(event.getActionCommand())) {
            System.exit(0);
        }
    }

    private void readFormData() {
        String str = textArea1.getText();
        String[] listStr = str.split("\n");
        dirs = new ArrayList<>();
        for (int i = 0; i < listStr.length; i++) {
            dirs.add(listStr[i]);
        }

        str = textArea2.getText();
        listStr = str.split("\n");
        procedurs = new ArrayList<>();
        for (int i = 0; i < listStr.length; i++) {
            procedurs.add(listStr[i]);
        }

        str = textArea3.getText();
        listStr = str.split("\n");
        linesKeyB = new ArrayList<>();
        for (int i = 0; i < listStr.length; i++) {
            linesKeyB.add(listStr[i]);
        }

        str = textArea5.getText();
        listStr = str.split("\n");
        linesKeyA = new ArrayList<>();
        for (int i = 0; i < listStr.length; i++) {
            linesKeyA.add(listStr[i]);
        }

        str = textArea4.getText();
        listStr = str.split("\n");
        linesAdd = new ArrayList<>();
        for (int i = 0; i < listStr.length; i++) {
            linesAdd.add(listStr[i]);

        }
    }

    class PaintPanel extends JPanel {

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            //g.drawString("Draw block", 10, 20);
            //g.drawRect(10, 40, 50, 80);
        }
    }
}
