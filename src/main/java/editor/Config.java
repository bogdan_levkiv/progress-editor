package editor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;

public class Config {

    public static List<String> dirs;
    public static List<String> procedurs;
    public static List<String> linesKeyB;
    public static List<String> linesKeyA;
    public static List<String> linesAdd;

    public static void Load(String file_lines, String codePageFile) throws IOException {

        dirs = new ArrayList<String>();
        procedurs = new ArrayList<String>();
        linesKeyB = new ArrayList<String>();
        linesKeyA = new ArrayList<String>();
        linesAdd = new ArrayList<String>();

        boolean dirsB = false;
        boolean procedursB = false;
        boolean lines_keyBB = false;
        boolean lines_keyBA = false;
        boolean lines_addB = false;

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file_lines), codePageFile));
        String l;
        while ((l = reader.readLine()) != null) {
            if (l.equals("")) {
                continue;
            }
            if (l.equals("[dirs]")) {
                dirsB = true;
                procedursB = false;
                lines_keyBB = false;
                lines_keyBA = false;
                lines_addB = false;
                continue;
            } else if (l.equals("[procedurs]")) {
                dirsB = false;
                procedursB = true;
                lines_keyBB = false;
                lines_keyBA = false;
                lines_addB = false;
                continue;
            } else if (l.equals("[lines_key_before]")) {
                dirsB = false;
                procedursB = false;
                lines_keyBB = true;
                lines_keyBA = false;
                lines_addB = false;
                continue;
            } else if (l.equals("[lines_key_after]")) {
                dirsB = false;
                procedursB = false;
                lines_keyBB = false;
                lines_keyBA = true;
                lines_addB = false;
                continue;
            } else if (l.equals("[lines_add]")) {
                dirsB = false;
                procedursB = false;
                lines_keyBB = false;
                lines_keyBA = false;
                lines_addB = true;
                continue;
            }
            if (dirsB) {
                dirs.add(l);
            } else if (procedursB) {
                procedurs.add(l);
            } else if (lines_keyBB) {
                linesKeyB.add(l);
            } else if (lines_keyBA) {
                linesKeyA.add(l);
            } else if (lines_addB) {
                linesAdd.add(l);
            }
        }
        if (reader
                != null) {
            reader.close();
        }
    }

    public static void Save(String file_lines, String codePageFile) throws IOException {

        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file_lines), codePageFile));
        writer.write("[dirs]\n");
        for (String str : dirs) {
            writer.write(str);
            writer.write("\n");
        }
        writer.write("\n[procedurs]\n");
        for (String str : procedurs) {
            writer.write(str);
            writer.write("\n");
        }
        writer.write("\n[lines_key_before]\n");
        for (String str : linesKeyB) {
            writer.write(str);
            writer.write("\n");
        }
        writer.write("\n[lines_key_after]\n");
        for (String str : linesKeyA) {
            writer.write(str);
            writer.write("\n");
        }
        writer.write("\n[lines_add]\n");
        for (String str : linesAdd) {
            writer.write(str);
            writer.write("\n");
        }
        writer.write("\n");

        if (writer != null) {
            writer.close();
        }
    }
}
