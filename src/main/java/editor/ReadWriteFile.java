package editor;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class ReadWriteFile {

    String inFile;
    String outFile;
    String codePageFile;
    List<String> listFile = null;
    List<String> listIncludes = null;
    BufferedReader inBR = null;
    PrintWriter outPW = null;

    public ReadWriteFile(String inFile, String outFile, String codePageFile) {
        this.inFile = new String(inFile);
        this.outFile = new String(outFile);
        this.codePageFile = new String(codePageFile);
    }

    public boolean findLine(List<String> s, List<String> inProcedure, boolean equals) throws IOException {
        boolean finded = false;
        String procedure = "";
        try {
            inBR = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), codePageFile));
            String l;
            listIncludes = new ArrayList<>();
            while ((l = inBR.readLine()) != null) {
                if (l.matches("(?i:.*procedure.*)")) {
                    procedure = l;
                }
                if ((equals) && (l.equalsIgnoreCase(s.get(0))) && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add(l);
                    finded = true;
                } else if ((!equals) && (matchesList(l, s, 0)) && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add(l);
                    finded = true;
                }
            }
            if (inBR != null) {
                inBR.close();
            }
            Iterator iteratorIncludes = listIncludes.iterator();
            while (iteratorIncludes.hasNext()) {
                System.out.println(iteratorIncludes.next());
            }
        } finally {
            if (inBR != null) {
                inBR.close();
            }
        }
        return finded;
    }

    public boolean replace(List<String> s, List<String> s2, List<String> inProcedure, boolean equals) throws IOException {
        boolean removed = false;
        String procedure = "";
        try {
            inBR = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), codePageFile));
            String l;
            listFile = new ArrayList<>();
            listIncludes = new ArrayList<>();

            while ((l = inBR.readLine()) != null) {
                if (l.matches("(?i:.*procedure.*)")) {
                    procedure = l;
                }
                if ((equals) && (l.equalsIgnoreCase(s.get(0))) 
                        && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add(l);
                    removed = true;
                } else if ((!equals) && (matchesList(l, s, 0)) && !isComment(l)
                        && (matchesList(procedure, inProcedure, 0))) {
                    for (String line : s2) {
                        listFile.add(line);
                    }
                    listIncludes.add(l);
                    removed = true;
                } else {
                    listFile.add(l);
                }
            }
            if (inBR != null) {
                inBR.close();
            }
            outPW = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outFile), codePageFile));
            Iterator iteratorFile = listFile.iterator();
            while (iteratorFile.hasNext()) {
                //System.out.println(iterator.next());
                outPW.println(iteratorFile.next());
            }
            Iterator iteratorIncludes = listIncludes.iterator();
            while (iteratorIncludes.hasNext()) {
                System.out.println(iteratorIncludes.next());
            }
            if (outPW != null) {
                outPW.close();
            }
        } finally {
            if (inBR != null) {
                inBR.close();
            }
            if (outPW != null) {
                outPW.close();
            }
        }
        return removed;
    }

    public boolean removeLine(List<String> s, List<String> inProcedure, boolean equals) throws IOException {
        boolean removed = false;
        String procedure = "";
        try {
            inBR = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), codePageFile));
            String l;
            listFile = new ArrayList<>();
            listIncludes = new ArrayList<>();

            while ((l = inBR.readLine()) != null) {
                if (l.matches("(?i:.*procedure.*)")) {
                    procedure = l;
                }
                if ((equals) && (l.equalsIgnoreCase(s.get(0))) 
                        && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add(l);
                    removed = true;
                } else if ((!equals) && (matchesList(l, s, 0)) && !isComment(l)
                        && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add(l);
                    removed = true;
                } else {
                    listFile.add(l);
                }
            }
            if (inBR != null) {
                inBR.close();
            }
            outPW = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outFile), codePageFile));
            Iterator iteratorFile = listFile.iterator();
            while (iteratorFile.hasNext()) {
                //System.out.println(iterator.next());
                outPW.println(iteratorFile.next());
            }
            Iterator iteratorIncludes = listIncludes.iterator();
            while (iteratorIncludes.hasNext()) {
                System.out.println(iteratorIncludes.next());
            }
            if (outPW != null) {
                outPW.close();
            }
        } finally {
            if (inBR != null) {
                inBR.close();
            }
            if (outPW != null) {
                outPW.close();
            }
        }
        return removed;
    }

    public boolean removeLines(List<String> s, List<String> inProcedure) throws IOException {
        boolean removed = false;
        String procedure = "";
        String[] tempStr = new String[s.size()];
        int index = 0;
        try {
            inBR = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), codePageFile));
            String l;
            listFile = new ArrayList<>();
            listIncludes = new ArrayList<>();
            while ((l = inBR.readLine()) != null) {
                if (l.matches("(?i:.*procedure.*)")) {
                    procedure = l;
                }
                int i = matchesListLines(l, s, 0);
                if ((i == index) && !isComment(l) && (matchesList(procedure, inProcedure, 0))) {
                    System.out.println(i + ": " + l);
                    tempStr[index] = l;
                    index++;
                    listIncludes.add(l);
                    removed = true;
                } else {
                    if (index != 0) {
                        if (index < s.size()) {
                            for (int j = 0; j < index; j++) {
                                listFile.add(tempStr[j]);
                            }
                        }
                        for (int j = 0; j < tempStr.length; j++) {
                            tempStr[j] = "";
                        }
                        index = 0;
                    }
                    listFile.add(l);
                }
            }
            if (inBR != null) {
                inBR.close();
            }
            outPW = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outFile), codePageFile));
            Iterator iteratorFile = listFile.iterator();
            while (iteratorFile.hasNext()) {
                //System.out.println(iterator.next());
                outPW.println(iteratorFile.next());
            }
            Iterator iteratorIncludes = listIncludes.iterator();
            while (iteratorIncludes.hasNext()) {
                System.out.println(iteratorIncludes.next());
            }
            if (outPW != null) {
                outPW.close();
            }
        } finally {
            if (inBR != null) {
                inBR.close();
            }
            if (outPW != null) {
                outPW.close();
            }
        }
        return removed;
    }

    public boolean commentLine(List<String> s, List<String> inProcedure) throws IOException {
        String procedure = "";
        boolean commented = false;

        try {
            inBR = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), codePageFile));
            String l;
            listFile = new ArrayList<>();
            listIncludes = new ArrayList<>();

            while ((l = inBR.readLine()) != null) {
                if (l.matches("(?i:.*procedure.*)")) {
                    procedure = l;
                }
                if ((matchesList(l, s, 0)) && !isComment(l)
                        && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add("/* " + l + " */");
                    listFile.add("/* " + l + " */");
                    commented = true;
                } else {
                    listFile.add(l);
                }
            }
            if (inBR != null) {
                inBR.close();
            }
            outPW = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outFile), codePageFile));
            Iterator iteratorFile = listFile.iterator();
            while (iteratorFile.hasNext()) {
                //System.out.println(iterator.next());
                outPW.println(iteratorFile.next());
            }
            Iterator iteratorIncludes = listIncludes.iterator();
            while (iteratorIncludes.hasNext()) {
                System.out.println(iteratorIncludes.next());
            }
            if (outPW != null) {
                outPW.close();
            }
        } finally {
            if (inBR != null) {
                inBR.close();
            }
            if (outPW != null) {
                outPW.close();
            }
        }
        return commented;
    }

    public boolean uncommentLine(List<String> s, List<String> inProcedure) throws IOException {
        String procedure = "";
        boolean uncommented = false;

        try {
            inBR = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), codePageFile));
            String l;
            listFile = new ArrayList<>();
            listIncludes = new ArrayList<>();

            while ((l = inBR.readLine()) != null) {
                if (l.matches("(.*)[Pp][Rr][Oo][Cc][Ee][Dd][Uu][Rr][Ee](.*)")) {
                    procedure = l;
                }
                if ((matchesList(l, s, 0)) && isComment(l)
                        && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add(l);
                    listFile.add(cleanComment(l));
                    uncommented = true;
                } else {
                    listFile.add(l);
                }
            }
            if (inBR != null) {
                inBR.close();
            }
            outPW = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outFile), codePageFile));
            Iterator iteratorFile = listFile.iterator();
            while (iteratorFile.hasNext()) {
                //System.out.println(iterator.next());
                outPW.println(iteratorFile.next());
            }
            Iterator iteratorIncludes = listIncludes.iterator();
            while (iteratorIncludes.hasNext()) {
                System.out.println(iteratorIncludes.next());
            }
            if (outPW != null) {
                outPW.close();
            }
        } finally {
            if (inBR != null) {
                inBR.close();
            }
            if (outPW != null) {
                outPW.close();
            }
        }
        return uncommented;
    }

    public boolean beforeLine(List<String> s, List<String> addS, List<String> inProcedure, boolean equals) throws IOException {
        boolean after = false;
        return linesAdd(s, addS, after, inProcedure, equals);
    }

    public boolean afterLine(List<String> s, List<String> addS, List<String> inProcedure, boolean equals) throws IOException {
        boolean after = true;
        return linesAdd(s, addS, after, inProcedure, equals);
    }

    private boolean linesAdd(List<String> s, List<String> addS, boolean before, List<String> inProcedure, boolean equals) throws IOException {
        boolean linesAdds = false;
        String procedure = "";

        try {
            inBR = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), codePageFile));
            String l;
            listFile = new ArrayList<>();
            listIncludes = new ArrayList<>();

            while ((l = inBR.readLine()) != null) {
                if (l.matches("(.*)[Pp][Rr][Oo][Cc][Ee][Dd][Uu][Rr][Ee](.*)")) {
                    procedure = l;
                }
                // !isComment(l)
                if ((equals) && (l.equalsIgnoreCase(s.get(0))) 
                        && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add(l);
                    if (!before) {
                        listFile.add(l);
                    }
                    for (int i = 0; i < addS.size(); i++) {
                        listFile.add(addS.get(i));
                    }
                    if (before) {
                        listFile.add(l);
                    }
                    linesAdds = true;
                } else if ((!equals) && (matchesList(l, s, 0)) 
                        && (matchesList(procedure, inProcedure, 0))) {
                    listIncludes.add(l);
                    if (!before) {
                        listFile.add(l);
                    }
                    for (int i = 0; i < addS.size(); i++) {
                        listFile.add(addS.get(i));
                    }
                    if (before) {
                        listFile.add(l);
                    }
                    linesAdds = true;

                } else {
                    listFile.add(l);
                }
            }
            if (inBR != null) {
                inBR.close();
            }
            outPW = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outFile), codePageFile));
            Iterator iteratorFile = listFile.iterator();
            while (iteratorFile.hasNext()) {
                //System.out.println(iterator.next());
                outPW.println(iteratorFile.next());
            }
            Iterator iteratorIncludes = listIncludes.iterator();
            while (iteratorIncludes.hasNext()) {
                System.out.println(iteratorIncludes.next());
            }
            if (outPW != null) {
                outPW.close();
            }
        } finally {
            if (inBR != null) {
                inBR.close();
            }
            if (outPW != null) {
                outPW.close();
            }
        }
        return linesAdds;
    }

    private String cleanComment(String s) {
        boolean startComment = false;
        boolean finishComment = false;

        String copyS = s;
        if (s.matches("(.*)/\\* (.*)")) {
            s = s.replace("/* ", "");
            startComment = true;
        } else if (s.matches("(.*)/\\*(.*)")) {
            s = s.replace("/*", "");
        }
        if (s.matches("(.*) \\*/(.*)")) {
            s = s.replace(" */", "");
            finishComment = true;
        } else if (s.matches("(.*)\\*/(.*)")) {
            s = s.replace("*/", "");
            finishComment = true;
        }
        if (startComment && finishComment) {
            return s;
        } else {
            return copyS;
        }
    }

    /**
     * matchesList
     * 
     * @return line matches all list elements
     */
    private boolean matchesList(String line, List<String> ls, int i) {
        boolean includes = false;
        String repl = ls.get(i).replace("(", "\\(");
        repl = repl.replace(")", "\\)");
        repl = repl.replace("/", "\\/");
        repl = repl.replace("\"", "\\\"");
        repl = repl.replace("{", "\\{");
        repl = repl.replace("}", "\\}");
        repl = repl.replace("*", "\\*");
        repl = repl.replace("?", "\\?");
        if ((i < (ls.size() - 1)) && (line.matches("(?i:.*" + repl + ".*)"))) {
            includes = matchesList(line, ls, i + 1);
        } else if ((i < ls.size()) && (line.matches("(?i:.*" + repl + ".*)"))) {
            includes = true;
        }
        return includes;
    }

    /**
     * matchesListLines
     * 
     * @return line included in list
     */
    private int matchesListLines(String line, List<String> ls, int i) {
        int includes = -1;
        String repl = ls.get(i).replace("(", "\\(");
        repl = repl.replace(")", "\\)");
        repl = repl.replace("/", "\\/");
        repl = repl.replace("\"", "\\\"");
        repl = repl.replace("{", "\\{");
        repl = repl.replace("}", "\\}");
        repl = repl.replace("*", "\\*");
        repl = repl.replace("?", "\\?");
        if ((i < ls.size()) && (line.matches("(?i:.*" + repl + ".*)"))) {
            return i;
        } else if (i < (ls.size() - 1)) {
            includes = matchesListLines(line, ls, i + 1);
        }
        return includes;
    }

    private boolean isComment(String line) {
        if (((line.matches("(.*)/\\*(.*)"))) && (line.matches("(.*)\\*/(.*)"))) {
            return true;
        }
        return false;
    }

    public String getInFile() {
        return inFile;
    }

    public String getOutFile() {
        return outFile;
    }
}
