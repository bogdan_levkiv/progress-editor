package editor;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.IOException;

import java.io.File;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

public class Main {

    public Main() throws IOException {
        Gui frame = new Gui();
        SwingUtilities.invokeLater(frame);
    }

    public static void main(String[] args) {
        try {
            Main main1 = new Main();
        } catch (IOException ioe) {
            System.out.println("dont find directory");
        }
    }
}
